package com.example.trong.searchfor.listenner;

import com.example.trong.searchfor.common.Place;

public interface PlaceListenner {
    void onDetail(Place place);
    void onMapViewChange();
}
