package com.example.trong.searchfor.view;

import android.os.Bundle;

import com.example.trong.searchfor.R;
import com.example.trong.searchfor.view.base.BaseActivity;


public class ListPlaceActivity extends BaseActivity {


    @Override
    protected int getLayoutResources() {
        return R.layout.activity_list_place;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {

    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }
}
